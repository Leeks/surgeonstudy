from django.conf import settings
import braintree

braintree.Configuration.configure(
    settings.BRAINTREE_ENVIRONMENT,
    settings.BRAINTREE_MERCHANT_ID,
    settings.BRAINTREE_PUBLIC_KEY,
    settings.BRAINTREE_PRIVATE_KEY
)
