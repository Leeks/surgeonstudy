django.jQuery(document).ready(function () {
    django.jQuery(".field-is_correct > input[type='checkbox']").on("click", function () {

        $action = django.jQuery(django.jQuery(this).children()[0]);
        current_state = $action.is(':checked');

        // Mark all checkboxes as false
        django.jQuery('.field-is_correct').each(function (index) {
            var $temp = django.jQuery(django.jQuery(this).children()[0]);
            $temp.val(false);
            $temp.prop('checked', false);
        });

        var $current = django.jQuery(this);
        $current.val(true);
        $current.prop('checked', true);
    });
});
