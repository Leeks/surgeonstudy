from typing import List, Dict, TypeVar
from functools import reduce
from django.contrib.auth import get_user_model

import braintree
from braintree import exceptions


def sync_add_ons() -> List[braintree.AddOn]:
    from .models import AddOn

    braintree_add_ons = braintree.AddOn.all()
    braintree_add_on_ids = [add_on.id for add_on in braintree_add_ons]

    for braintree_add_on in braintree_add_ons:
        AddOn.objects.update_or_create(
            braintree_id=braintree_add_on.id,
            defaults={'name': braintree_add_on.name}
        )

    AddOn.objects.exclude(braintree_id__in=braintree_add_on_ids).delete()
    return braintree_add_ons


def sync_plan_add_ons(plan, braintree_plan: braintree.Subscription=None) -> List[braintree.Plan]:
    from .models import AddOn

    try:
        if braintree_plan is None:
            braintree_plan = braintree.Subscription.find(plan.braintree_id)
    except braintree.exceptions.NotFoundError:
        plan.delete()
        return None

    braintree_add_ons = braintree_plan.add_ons
    braintree_add_on_ids = [add_on.id for add_on in braintree_add_ons]

    plan.add_ons = AddOn.objects.filter(braintree_id__in=braintree_add_on_ids)
    return braintree_add_ons


def sync_plans() -> List[braintree.Plan]:
    from .models import Plan

    braintree_plans = braintree.Plan.all()
    braintree_plan_ids = [add_on.id for add_on in braintree_plans]

    for braintree_plan in braintree_plans:
        plan, created = Plan.objects.update_or_create(
            braintree_id=braintree_plan.id,
            defaults={'name': braintree_plan.name}
        )
        sync_plan_add_ons(plan, braintree_plan)

    Plan.objects.exclude(braintree_id__in=braintree_plan_ids).delete()
    return braintree_plans


def sync_subscription_add_ons(subscription, braintree_subscription: braintree.Subscription=None) -> List[braintree.AddOn]:
    from .models import AddOn

    try:
        if braintree_subscription is None:
            braintree_subscription = braintree.Subscription.find(subscription.braintree_id)
    except braintree.exceptions.NotFoundError:
        subscription.delete()
        return None

    braintree_add_ons = braintree_subscription.add_ons
    braintree_add_on_ids = [add_on.id for add_on in braintree_add_ons]

    subscription.add_ons = AddOn.objects.filter(braintree_id__in=braintree_add_on_ids)
    return braintree_add_ons


def sync_customer_subscriptions(customer, braintree_customer: braintree.Customer=None) -> List[braintree.Subscription]:
    from .models import Plan, Subscription

    try:
        if braintree_customer is None:
            braintree_customer = braintree.Customer.find(customer.braintree_id)
    except braintree.exceptions.NotFoundError:
        customer.delete()
        return None

    braintree_subscriptions = reduce(
        list.__add__,
        [payment_methods.subscriptions for payment_methods in braintree_customer.payment_methods]
    )
    braintree_subscription_ids = [subscription.id for subscription in braintree_subscriptions]

    for braintree_subscription in braintree_subscriptions:
        subscription, created = customer.subscription_set.update_or_create(
            braintree_id=braintree_subscription.id,
            customer=customer,
            defaults={
                'plan': Plan.objects.get(braintree_id=braintree_subscription.plan_id),
                'status': braintree_subscription.status
            }
        )
        sync_subscription_add_ons(subscription, braintree_subscription)

    Subscription.objects.filter(customer=customer).exclude(braintree_id__in=braintree_subscription_ids).delete()
    return braintree_subscriptions


def sync_customer(customer, braintree_customer: braintree.Customer = None) -> braintree.Customer:
    try:
        if braintree_customer is None:
            braintree_customer = braintree.Customer.find(customer.braintree_id)

        # Update customer values

        sync_customer_subscriptions(customer, braintree_customer)
    except braintree.exceptions.NotFoundError:
        customer.delete()
        return None

    return braintree_customer


def sync_customers() -> List[braintree.Customer]:
    from .models import Customer

    braintree_customers = braintree.Customer.all()

    for braintree_customer in braintree_customers:
        user, created = get_user_model().objects.get_or_create(username=braintree_customer.id)
        Customer.objects.get_or_create(braintree_id=braintree_customer.id, subscriber=user)

    braintree_customer_ids = [customer.id for customer in braintree_customers]
    Customer.objects.exclude(braintree_id__in=braintree_customer_ids).delete()
    return braintree_customers


def sync() -> Dict[str, List[TypeVar('T', braintree.AddOn, braintree.Plan, braintree.Customer)]]:
    return {
        'add_ons': sync_add_ons(),
        'plans': sync_plans(),
        'customers': sync_customers()
    }
