from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^client_token/$', views.get_client_token, name='client_token'),
    url(r'^delete_payment_method/$', views.delete_payment_method, name='delete_payment_method'),
]
