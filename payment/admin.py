from django.contrib import admin
from django.contrib.admin.views.decorators import staff_member_required
from django.http import Http404
from django.http.response import HttpResponseRedirect
from django.conf.urls import url

from .models import AddOn, Plan, Customer, Subscription
from .sync import sync_add_ons, sync_plans, sync_customers, sync_customer


class ReadOnlyModel(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(ReadOnlyModel, self).get_actions(request)
        del actions['delete_selected']
        return actions


class AddOnAdmin(ReadOnlyModel):
    fields = ('braintree_id', 'name')
    readonly_fields = fields

    @staticmethod
    @staff_member_required
    def admin_sync_add_ons(request):
        sync_add_ons()
        return HttpResponseRedirect(request.META['HTTP_REFERER'])

    def get_urls(self):
        urls = super(AddOnAdmin, self).get_urls()
        my_urls = [
            url(r'^sync/', self.admin_sync_add_ons)
        ]
        return my_urls + urls


class PlanAdmin(ReadOnlyModel):
    fields = ('braintree_id', 'name', 'add_ons')
    readonly_fields = fields

    @staticmethod
    @staff_member_required
    def admin_sync_plans(request):
        sync_plans()
        return HttpResponseRedirect(request.META['HTTP_REFERER'])

    def get_urls(self):
        urls = super(PlanAdmin, self).get_urls()
        my_urls = [
            url(r'^sync/', self.admin_sync_plans)
        ]
        return my_urls + urls


class CustomerAdmin(ReadOnlyModel):
    fields = ('braintree_id', 'subscriber')
    readonly_fields = fields

    def get_actions(self, request):
        actions = super(CustomerAdmin, self).get_actions(request)
        actions['sync_customer'] = (self.admin_sync_customer_action, 'sync_customer', 'Sync customer')
        return actions

    @staticmethod
    @staff_member_required
    def admin_sync_customers(request):
        sync_customers()
        return HttpResponseRedirect(request.META['HTTP_REFERER'])

    @staticmethod
    def admin_sync_customer_action(modeladmin, request, queryset):
        for customer in queryset:
            sync_customer(customer)

    @staticmethod
    @staff_member_required
    def admin_sync_customer(request, **kwargs):
        try:
            customer = Customer.objects.get(pk=kwargs['pk'])
            sync_customer(customer)
            return HttpResponseRedirect(request.META['HTTP_REFERER'])
        except Customer.DoesNotExist:
            raise Http404('Customer with id {} not found'.format(kwargs['pk']))

    def get_urls(self):
        urls = super(CustomerAdmin, self).get_urls()
        my_urls = [
            url(r'^sync/', self.admin_sync_customers),
            url(r'^(?P<pk>\d+)/change/sync/', self.admin_sync_customer)
        ]
        return my_urls + urls


class SubscriptionAdmin(ReadOnlyModel):
    fields = ('braintree_id', 'customer', 'plan', 'add_ons', 'status')
    readonly_fields = fields


admin.site.register(AddOn, AddOnAdmin)
admin.site.register(Plan, PlanAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
