from braintree.exceptions import NotFoundError
from rest_framework import generics, mixins, views, exceptions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
import braintree

from .models import Customer


@api_view()
@permission_classes([IsAuthenticated])
def get_client_token(request):
    customer, created = Customer.objects.get_or_create(braintree_id=request.user.username, subscriber=request.user)
    client_token = braintree.ClientToken.generate({
        'customer_id': customer.braintree_id
    })
    return Response({'client_token': client_token})


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def delete_payment_method(request):
    try:
        nonce = request.data['payment_method_nonce']
    except KeyError:
        raise exceptions.ValidationError({
            'code': 'missing_data',
            'detail': 'Missing key \'payment_method_nonce\''
        })

    payment_method_gateway = braintree.Configuration.gateway().payment_method
    try:
        if nonce is None or nonce.strip() == "":
            raise exceptions.ValidationError({
                'code': 'invalid_nonce',
                'detail': 'invalid payment method nonce'
            })
        response = payment_method_gateway.config.http().get(
            payment_method_gateway.config.base_merchant_path() + "/payment_methods/from_nonce/" + nonce
        )
        payment_method = payment_method_gateway._parse_payment_method(response)
    except NotFoundError:
        raise exceptions.NotFound({
            'code': 'invalid_nonce',
            'detail': 'payment method with nonce " + nonce + " locked, consumed or not found'
        })

    if not payment_method.subscriptions:
        result = braintree.PaymentMethod.delete(payment_method.token)
        if not result.is_success:
            raise exceptions.ValidationError({
                'code': 'delete_failed',
                'detail': 'failed to delete payment method'
            })
    else:
        raise exceptions.ValidationError({
            'code': 'payment_method_default',
            'detail': 'payment method is the default payment method'
        })

    return Response(status=204)

