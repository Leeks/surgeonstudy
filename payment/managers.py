from django.db import models
import braintree

from .sync import sync_customer


class CustomerManager(models.Manager):
    def get_or_create(self, defaults=None, **kwargs):
        customer, created = super().get_or_create(defaults, **kwargs)
        if created:
            braintree_customer = braintree.Customer.create({
                'id': kwargs['braintree_id']
            })
            if not braintree_customer.is_success:
                braintree_customer = braintree.Customer.find(kwargs['braintree_id'])

            sync_customer(braintree_customer, customer)

        return customer, created
