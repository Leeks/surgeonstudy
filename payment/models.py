from django.conf import settings
from django.db import models
from model_utils.models import TimeStampedModel

from .managers import CustomerManager


class AddOn(TimeStampedModel):
    braintree_id = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name.__str__()


class Plan(TimeStampedModel):
    braintree_id = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=50, unique=True)
    add_ons = models.ManyToManyField(AddOn)

    def __str__(self):
        return self.name.__str__()


class Customer(TimeStampedModel):
    objects = CustomerManager()

    braintree_id = models.CharField(max_length=50, unique=True)
    subscriber = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.subscriber.__str__()


class Subscription(TimeStampedModel):
    braintree_id = models.CharField(max_length=50, unique=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    add_ons = models.ManyToManyField(AddOn)
    status = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return '{}: {} ({})'.format(self.customer.__str__(), self.plan.__str__(), self.status.__str__())
