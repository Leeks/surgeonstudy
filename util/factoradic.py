def permutate(xs, fac):
    xs = list(xs)
    r = []

    for f in fac:
        try:
            r.append(xs.pop(f))
        except IndexError:
            break

    if xs:
        r.extend(xs)

    return r


def factoradic(b10):
    from itertools import count

    if b10 == 0:
        return [0]

    res = [0]
    q = b10

    for i in count(2):
        q, r = divmod(q, i)
        res.append(r)
        if q == 0:
            break

    res.reverse()
    return res