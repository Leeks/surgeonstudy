from unittest import TestCase

from bundle.models import Bundle, Purchase
from study.models import QuestionSet, Question, Choice, User, Category, Exam


class StandardTestUtilsMixin(TestCase):
    def assertDictContainsSubset(self, outer: dict, inner: dict, msg=None):
        for k, v in inner.items():
            ov = outer.get(k)
            if v != ov:
                msg = self._formatMessage(
                    msg,
                    'Outer dict contains value {} for key {} instead of the expected value {}'
                        .format(repr(ov), repr(k), repr(v))
                )
                raise self.failureException(msg)


class AuthRequiredTestMixin(object):
    auth_tests = None

    def test_auth_required_endpoints(self):
        old_creds = self.client._credentials
        self.client.credentials()

        if self.auth_tests is not None:
            tests = self.auth_tests
        else:
            tests = self.get_auth_tests()

        for url in tests.keys():
            for method in tests[url]:
                if method == 'get':
                    res = self.client.get(url)
                elif method == 'post':
                    res = self.client.post(url)
                elif method == 'put':
                    res = self.client.put(url)
                elif method == 'patch':
                    res = self.client.patch(url)
                else:
                    raise Exception('Unknown method {}'.format(method))
                self.assertIn(res.status_code, [401, 403])

        self.client.credentials(**old_creds)

    def get_auth_tests(self):
        raise NotImplementedError()


class TestDataMixin(object):
    def setUp(self):
        # create questions
        category = Category.objects.create(name='Pick Me', slug='pick-me')
        self.questions = [Question.objects.create(prompt=p, category=category) for p in
                          ['Pick one.', 'Pick two.', 'Pick three.', 'Pick four.',
                           'Pick five.']]

        # set answer choices for questions
        for i, q in enumerate(self.questions):
            for num in range(i, i + 4):
                Choice.objects.create(question=q,
                                      text="I'm number {0}".format(num),
                                      is_correct=(True if num == (i + 1) else False))

        # add questions to questionset
        self.bundle = Bundle.objects.create(name='TestQuestions', slug='test-questions', description='Test Questions',
                                            price=100)

        self.questionset = QuestionSet.objects.create(name='QuestionSet1', slug='question-set-1')
        self.questionset.questions.add(*self.questions)
        self.questionset.bundles.add(self.bundle)

        self.user_super = User.objects.create(username='superuser', is_staff=True, is_superuser=True)
        self.user_staff = User.objects.create(username='staffuser', is_staff=True)
        self.user_without_purchases = User.objects.create(username='nopurchases')
        self.user_with_purchases = User.objects.create(username='purchases')
        self.user_with_purchases2 = User.objects.create(username='purchases2')
        Purchase.objects.create(user=self.user_with_purchases, bundle=self.bundle)
        Purchase.objects.create(user=self.user_with_purchases2, bundle=self.bundle)
        super(TestDataMixin, self).setUp()


class ExamSetupMixin(object):
    def setUp(self):
        super(ExamSetupMixin, self).setUp()
        self.exam = Exam.objects.start_new(
            self.user_with_purchases,
            question_count=2,
            question_queryset=Question.objects.filter(prompt__in=['Pick one.', 'Pick two.'])
        )