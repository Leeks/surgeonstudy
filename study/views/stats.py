from rest_framework import viewsets
from rest_framework.generics import get_object_or_404

from study.models import QuestionStats, Question
from study.serializers import QuestionStatsSerializer


class QuestionStatsViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = QuestionStatsSerializer

    def get_queryset(self, *args, **kwargs):
        user = self.request.user
        return QuestionStats.objects.raw("""
SELECT
  qs.id,
  p.user_id,
  study_question.id as question_id,
  coalesce(qs.times_correct, 0) as times_correct,
  coalesce(qs.times_incorrect, 0) as times_incorrect
FROM bundle_purchase p
LEFT JOIN bundle_bundle_questionsets
  ON p.bundle_id = bundle_bundle_questionsets.bundle_id
LEFT JOIN study_questionset_questions
  ON bundle_bundle_questionsets.questionset_id = study_questionset_questions.questionset_id
LEFT JOIN study_question
  ON study_questionset_questions.question_id = study_question.id
LEFT JOIN study_questionstats qs
  ON study_question.id = qs.question_id
WHERE p.user_id = %s
""", [user.id])

    def get_object(self):
        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        question = get_object_or_404(Question, **filter_kwargs)
        obj, _ = QuestionStats.objects.get_or_create(user=self.request.user, question=question)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj