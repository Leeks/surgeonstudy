from rest_framework import generics, mixins, views, exceptions, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from study.models import Question, CurrentQuestion, Choice
from study.serializers import QuestionSerializer
from study.stats import StatsManager


class QuickStudyView(views.APIView):
    def get_queryset(self):
        return Question.objects.for_user(self.request.user)

    def get_object(self):
        """Gets the users current question or sets one if there isn't one"""
        user = self.request.user

        return CurrentQuestion.objects.prefetch_related(
            'question',
            'question__choices',
            'question__category'
        ).filter(user=user).first()

    def get_object_or_create(self):
        user = self.request.user

        cq = self.get_object()

        if cq is None:
            q = self.get_queryset().order_by('?').first()
            if q is None:
                raise exceptions.ValidationError({
                    'code': 'no_questions',
                    'detail': 'User does not have access to any questions to start an exam.'
                })
            cq = CurrentQuestion.objects.prefetch_related(
                'question',
                'question__choices',
                'question__category'
            ).create(user=user, question=q)

        return cq

    def get_object_or_404(self):
        cq = self.get_object()

        if cq is None:
            raise exceptions.NotFound('No quick study question')
        else:
            return cq

    def get(self, request):
        cq = self.get_object_or_create()
        qs = QuestionSerializer(instance=cq.question, show_answers=False, permutation_no=cq.permutation_no)
        data = qs.data
        data['selected_choice'] = cq.selected_choice_id
        return Response(data)

    def put(self, request):
        try:
            selected_choice = request.data['selected_choice']
            if selected_choice is not None:
                selected_choice = int(selected_choice)
        except TypeError:
            raise exceptions.ValidationError({
                'code': 'invalid_data',
                'detail': '\'selected_choice\' must be an Integer or null'
            })
        except KeyError:
            raise exceptions.ValidationError({
                'code': 'missing_data',
                'detail': 'Missing key \'selected_choice\''
            })

        cq = self.get_object_or_404()

        if selected_choice is None:
            choice = None
        else:
            try:
                choice = cq.question.choices.get(pk=selected_choice)
            except Choice.DoesNotExist:
                raise exceptions.ValidationError({
                    'code': 'invalid_choice',
                    'detail': 'Invalid answer choice'
                })

        cq.selected_choice = choice
        cq.save()

        qs = QuestionSerializer(instance=cq.question, show_answers=False, permutation_no=cq.permutation_no)
        data = qs.data
        data['selected_choice'] = cq.selected_choice_id
        return Response(data)

    def post(self, request):
        user = request.user

        cq = self.get_object_or_404()

        if cq.selected_choice_id is None:
            raise exceptions.ValidationError({
                'code': 'question_not_answered',
                'detail': 'The question has not been answered'
            })
        else:
            StatsManager(user=user).question_answered(cq.question, cq.selected_choice)
            cq.delete()

        qs = QuestionSerializer(instance=cq.question, show_answers=True, permutation_no=cq.permutation_no)
        data = qs.data
        data['selected_choice'] = cq.selected_choice_id
        return Response(data)

    def delete(self, request):
        cq = self.get_object_or_404()

        cq.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
