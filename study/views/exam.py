from rest_framework import exceptions
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from study.models import Exam, ExamQuestion
from study.serializers import ExamQuestionSerializer, ExamSerializer


class ExamQuestionViewSet(mixins.ListModelMixin,
                          mixins.UpdateModelMixin,
                          mixins.RetrieveModelMixin,
                          viewsets.GenericViewSet):
    serializer_class = ExamQuestionSerializer
    lookup_field = 'no'
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        examquestions = ExamQuestion.objects.filter(exam=self.kwargs['exam_pk'], exam__user=self.request.user).prefetch_related(
            'exam',
            'question',
            'question__choices',
            'question__category',
        )
        if not examquestions:
            raise exceptions.NotFound('Exam not found')
        else:
            return examquestions


class ExamViewSet(viewsets.ModelViewSet):
    serializer_class = ExamSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('is_finished',)

    def get_queryset(self):
        return Exam.objects.filter(user=self.request.user)

    def create(self, request, *args, **kwargs):
        incomplete_exams = Exam.objects.filter(user=self.request.user, is_finished=False)
        if not incomplete_exams.exists():
            new_exam = Exam.objects.start_new(user=self.request.user)
            serializer = self.get_serializer(new_exam, many=False)
            return Response(serializer.data, status=201)
        else:
            raise exceptions.ValidationError({
                'code': 'incomplete_exam_already_exists',
                'detail': 'user can only have one unfinished exam'
            })

    def destroy(self, request, *args, **kwargs):
        if self.get_object().is_finished:
            raise exceptions.ValidationError({
                'code': 'exam_already_completed',
                'detail': 'cannot delete a finished exam'
            })
        return super(ExamViewSet, self).destroy(request, *args, **kwargs)