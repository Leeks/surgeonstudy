from functools import partial
from random import randrange

from rest_framework import exceptions
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from study.models import Question
from study.serializers import QuestionStudyListSerializer, QuestionStudyQuestionSerializer
from study.stats import StatsManager


class QuestionStudyViewSet(mixins.ListModelMixin,
                          mixins.UpdateModelMixin,
                          mixins.RetrieveModelMixin,
                          viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action == 'list':
            return QuestionStudyListSerializer
        elif self.action == 'retrieve':
            return QuestionStudyQuestionSerializer
        else:
            return partial(QuestionStudyQuestionSerializer, show_answers=True)

    def get_queryset(self):
        if self.action == 'list':
            return Question.objects.for_user(self.request.user).prefetch_related('category')
        else:
            return Question.objects.for_user(self.request.user).prefetch_related('category', 'choices')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        question_order = randrange(1, 10000)
        serializer = self.get_serializer(instance, rand_seed=question_order)
        data = serializer.data
        data['question_order'] = question_order
        data['selected_choice'] = None
        return Response(data)

    def update(self, request, *args, **kwargs):
        try:
            selected_choice = request.data['selected_choice']
            if selected_choice is not None:
                selected_choice = int(selected_choice)
        except TypeError:
            raise exceptions.ValidationError({
                'code': 'invalid_data',
                'detail': '\'selected_choice\' must be an Integer or null'
            })
        except KeyError:
            raise exceptions.ValidationError({
                'code': 'missing_data',
                'detail': 'Missing key \'selected_choice\''
            })

        try:
            question_order = int(request.data['question_order'])
        except TypeError:
            raise exceptions.ValidationError({
                'code': 'invalid_data',
                'detail': '\'question_order\' must be an Integer'
            })
        except KeyError:
            raise exceptions.ValidationError({
                'code': 'missing_data',
                'detail': 'Missing key \'question_order\''
            })

        question = self.get_object()
        serializer = self.get_serializer(question, rand_seed=question_order)
        data = serializer.data
        data['question_order'] = question_order
        data['selected_choice'] = selected_choice
        return Response(data)
