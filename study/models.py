import random
from hashlib import md5
from math import factorial

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import When, Case, Value, Avg, DecimalField
from django.utils import timezone
from django.utils.html import strip_tags
from rest_framework.exceptions import ValidationError
from sanitizer.models import SanitizedTextField, SanitizedCharField

from util.factoradic import factoradic, permutate

allowed_text_content = {
    'allowed_tags': [
        'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'img', 'strong', 'em', 'u', 'sub', 'sup', 's', 'ol', 'span',
        'ul', 'li', 'blockquote', 'div', 'hr', 'table', 'th', 'tr',
    ],
    'allowed_attributes': {
        'span': ['style'],
        'p': ['style'],
        'div': ['style'],
        'table': ['style'],
        'img': ['src', 'style'],
    },
    'allowed_styles': ['width', 'height', 'font-family', 'font-size', 'color', 'background-color',]
}


class Category(models.Model):
    name = models.TextField()
    slug = models.SlugField()

    class Meta:
        verbose_name_plural = 'categories'

    def __str__(self) -> str:
        return self.name


class Choice(models.Model):
    text = SanitizedCharField(max_length=255, null=False, blank=False, **allowed_text_content)
    question = models.ForeignKey(
        'Question',
        on_delete=models.CASCADE,
        related_name='choices',
    )
    is_correct = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.text


class QuestionSet(models.Model):
    name = models.TextField()
    slug = models.SlugField()
    questions = models.ManyToManyField('Question', related_name='questionsets')

    def __str__(self):
        return self.name


class QuestionManager(models.Manager):
    def for_user(self, user):
        return self.get_queryset()
        # if user.is_staff or user.is_superuser:
        #     return self.get_queryset()
        # else:
        #     return self.get_queryset().filter(questionsets__bundles__purchases__user=user)


class Question(models.Model):
    prompt = SanitizedTextField(null=False, blank=False, **allowed_text_content)
    critique = SanitizedTextField(null=True, blank=True, **allowed_text_content)
    category = models.ForeignKey(
        'Category',
        on_delete=models.CASCADE,
    )

    objects = QuestionManager()

    def __str__(self) -> str:
        return strip_tags(self.prompt)

    @property
    def shuffled_answer_choices(self):
        return self.choices.order_by('?')


class ExamQuestion(models.Model):
    exam = models.ForeignKey('Exam', on_delete=models.CASCADE, related_name='examquestions', db_index=True)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    no = models.SmallIntegerField(db_index=True)
    selected_choice = models.ForeignKey('Choice', null=True)

    class Meta:
        ordering = ['no']

    @property
    def permutation_no(self):
        """Permutation number of how the choices of this question should be shuffled in base-10"""
        max_perm = factorial(self.question.choices.count())
        e = int.from_bytes(md5(bytearray(self.exam_id + self.question_id + self.id)).digest(), 'big')
        return e % max_perm

    @property
    def shuffled_choices(self):
        f = factoradic(self.permutation_no)
        cs = [{'pk': c.id, 'text': c.text} for c in self.question.choices.all()]
        return permutate(cs, f)


class ExamManager(models.Manager):
    DEFAULT_QUESTION_COUNT = 250

    def current_exam(self, user):
        raise NotImplementedError

    def start_new(self, user, question_count=None, question_queryset=None, **kwargs):
        if question_count is None:
            question_count = self.DEFAULT_QUESTION_COUNT

        if question_queryset is None:
            question_queryset = Question.objects.for_user(user)

        questions = question_queryset.order_by('?').all()[:question_count]
        if not questions:
            raise ValidationError({
                'code': 'no_questions',
                'detail': 'User does not have access to any questions to start an exam.'
            })

        exam = self.create(user=user, **kwargs)

        ExamQuestion.objects.bulk_create(
            [ExamQuestion(exam=exam, question=question, no=i + 1) for i, question in enumerate(questions)])

        return exam

    @property
    def with_scores(self):
        qs = super(ExamManager, self).get_queryset()
        return qs.annotate(score=Avg(
            Case(
                When(records__selected_answer__is_correct=True, then=Value(100.0)),
                default=Value(0.0),
                output_field=DecimalField()
            )
        ))


class Exam(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    start_time = models.DateTimeField(auto_now_add=True)
    end_time = models.DateTimeField(null=True)
    score = models.DecimalField(null=True, decimal_places=2, max_digits=5)
    questions = models.ManyToManyField('Question', through='ExamQuestion', through_fields=('exam', 'question'))
    is_finished = models.BooleanField(default=False)

    objects = ExamManager()

    def __init__(self, *args, **kwargs):
        super(Exam, self).__init__(*args, **kwargs)
        self.was_finished = self.is_finished

    def save(self, *args, **kwargs):
        # Allow only one unfinished exam at a time.
        try:
            exam = Exam.objects.get(user=self.user, is_finished=False)
            if exam != self:
                raise ValidationError('User can only have one unfinished exam')
        except Exam.DoesNotExist:
            pass

        # Set end time on finish
        if self.is_finished and not self.was_finished:
            self.end_time = timezone.now()
            self.score = self._get_score()

        return super(Exam, self).save(*args, **kwargs)

    @property
    def percent_completion(self):
        total = self.examquestions.count()
        answered = self.examquestions.filter(selected_choice__isnull=False).count()
        return float(answered) / float(total)

    def _get_score(self):
        t = float(self.examquestions.count())
        c = float(self.examquestions.filter(selected_choice__is_correct=True).count())
        return c / t * 100

    def __str__(self):
        return '{}: {}'.format(self.user, self.start_time)


def random_permutation():
    return random.randint(0, 32767)


class CurrentQuestion(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    permutation_no = models.PositiveSmallIntegerField(default=random_permutation)
    selected_choice = models.ForeignKey('Choice', null=True, on_delete=models.CASCADE)


class GlobalQuestionStats(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    choices = models.ManyToManyField('Choice', through='GlobalChoiceStats')
    last_answered = models.DateTimeField(null=True)
    times_correct = models.PositiveIntegerField(default=0)
    times_incorrect = models.PositiveIntegerField(default=0)


class QuestionStats(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    choices = models.ManyToManyField('Choice', through='ChoiceStats')
    last_answered = models.DateTimeField(null=True)
    times_correct = models.PositiveIntegerField(default=0)
    times_incorrect = models.PositiveIntegerField(default=0)

    # @property
    # def accuracy(self):
    #     total = float(self.times_correct + self.times_incorrect)
    #     return self.times_correct / total


class GlobalChoiceStats(models.Model):
    choice = models.ForeignKey('Choice', on_delete=models.CASCADE)
    question_stats = models.ForeignKey('GlobalQuestionStats', on_delete=models.CASCADE, related_name='globalchoicestats')
    times_selected = models.PositiveIntegerField(default=0)


class ChoiceStats(models.Model):
    choice = models.ForeignKey('Choice', on_delete=models.CASCADE)
    question_stats = models.ForeignKey('QuestionStats', on_delete=models.CASCADE, related_name='choicestats')
    times_selected = models.PositiveIntegerField(default=0)
