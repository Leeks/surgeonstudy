from django.contrib import admin
from django import forms
from django.forms.utils import flatatt
from django.http.request import HttpRequest
from django.utils.decorators import method_decorator
from django.utils.functional import curry
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.db import models
from ckeditor.widgets import CKEditorWidget

csrf_protect_m = method_decorator(csrf_protect)
sensitive_post_parameters_m = method_decorator(sensitive_post_parameters())

from .models import (
    Choice,
    Category,
    Exam,
    Question,
    QuestionSet
)

for m in [Category, Exam]:
    admin.site.register(m)


@admin.register(QuestionSet)
class QuestionSetAdmin(admin.ModelAdmin):
    filter_horizontal = ('questions',)

# @admin.register(QuestionStats)
# class QuestionStats(admin.ModelAdmin):
#     fields = ('user', 'question', 'last_answered', 'correct_count', 'incorrect_count')


class IsCorrectRadioSelect(forms.Widget):
    def render(self, name, value, attrs=None):
        print(name, value)
        final_attrs = self.build_attrs(attrs, type='radio', name='choices-is_correct')
        selected_html = mark_safe(' checked="checked"') if value is True else ''
        output = format_html('<input{}{}>', flatatt(final_attrs), selected_html) + '</input>'
        return mark_safe(output)

    def is_checked(self):
        print(self)
        return self.value == self.choice_value


class AnswerChoiceInlineFormSet(forms.BaseInlineFormSet):
    def clean(self):
        super(AnswerChoiceInlineFormSet, self).clean()
        num_correct_choice_selected = 0
        for form in self.forms:
            if form.cleaned_data.get('is_correct') is True:
                num_correct_choice_selected += 1
        if num_correct_choice_selected < 1:
            raise forms.ValidationError('A correct choice must be selected')
        elif num_correct_choice_selected > 1:
            raise forms.ValidationError('Only one correct choice can be selected')


class AnswerChoiceInline(admin.TabularInline):
    model = Choice
    formset = AnswerChoiceInlineFormSet

    formfield_overrides = {
        models.CharField: {'widget': forms.TextInput(attrs={'size': '80'})},
    }

    def get_extra(self, request: HttpRequest, obj: Question=None, **kwargs):
        if obj is None:
            return 5
        else:
            remaining = 5 - obj.choices.count()
            if remaining < 0:
                return 0
            else:
                return remaining

    def get_formset(self, request: HttpRequest, obj=None, **kwargs):
        initial = []
        if request.method == "GET" and obj is None:
            initial.append({
                'is_correct': True,
            })
        formset = super(AnswerChoiceInline, self).get_formset(request, obj, **kwargs)
        formset.__init__ = curry(formset.__init__, initial=initial)
        return formset

    class Media:
        js = ('js/admin-inline-answer-choice.js',)


class CategoryFilter(admin.RelatedFieldListFilter):
    def __new__(cls, *args, **kwargs):
        instance = admin.FieldListFilter.create(*args, **kwargs)
        instance.title = 'category'
        return instance


class QuestionAdminForm(forms.ModelForm):
    prompt = forms.CharField(widget=CKEditorWidget())
    critique = forms.CharField(widget=CKEditorWidget(), required=False)

    class Meta:
        model = Question
        fields = ['prompt', 'critique', 'category']


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    form = QuestionAdminForm
    list_filter = (('category__name', CategoryFilter),)
    search_fields = ('@prompt', '@critique', '@category__name', '@choices__text')
    inlines = [
        AnswerChoiceInline,
    ]
