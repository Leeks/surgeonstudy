from random import shuffle, Random, randrange
from django.db.models import QuerySet
from rest_framework import serializers
from rest_framework.reverse import reverse

from study.models import User, Choice, Question, ExamQuestion, Exam, QuestionStats, ChoiceStats
from util.factoradic import permutate, factoradic


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ('id', 'text', 'is_correct')

    def __init__(self, show_answers=False, *args, **kwargs):
        super(ChoiceSerializer, self).__init__(*args, **kwargs)

        if not show_answers:
            self.fields.pop('is_correct')


class QuestionSerializer(serializers.ModelSerializer):
    category = serializers.StringRelatedField()
    choices = serializers.SerializerMethodField()

    class Meta:
        model = Question
        fields = ('prompt', 'category', 'choices')

    def __init__(self, *args, show_answers=False, permutation_no=None, rand_seed=None, **kwargs):
        super(QuestionSerializer, self).__init__(*args, **kwargs)
        self.permutation_no = permutation_no
        self.rand_seed = rand_seed
        self.show_answers = show_answers

    def get_choices(self, instance):
        choices = instance.choices.all()
        if self.permutation_no is not None:
            choices = permutate(choices, factoradic(self.permutation_no))
        elif self.rand_seed is not None:
            choices = list(choices)
            gen = Random(self.rand_seed)
            shuffle(choices, gen.random)
        s = ChoiceSerializer(instance=choices, show_answers=self.show_answers, many=True)
        return s.data


class QuestionStudyListSerializer(QuestionSerializer):
    class Meta:
        model = Question
        fields = ('pk', 'prompt', 'category')


class QuestionStudyQuestionSerializer(QuestionStudyListSerializer):
    class Meta:
        model = Question
        fields = ('pk', 'prompt', 'category', 'choices')


class ExamQuestionHyperlink(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            'exam_pk': obj.exam.pk,
            'no': obj.no
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)

    def get_object(self, view_name, view_args, view_kwargs):
        lookup_kwargs = {
            'exam': view_kwargs['exam_pk'],
            'no': view_kwargs['no']
        }
        return self.get_queryset().get(**lookup_kwargs)


class ExamQuestionSerializer(serializers.ModelSerializer):
    url = ExamQuestionHyperlink(view_name='examquestion-detail', lookup_field='no')
    question = serializers.SerializerMethodField()

    class Meta:
        model = ExamQuestion
        fields = ('no', 'url', 'question', 'selected_choice')
        read_only_fields = ('question', 'no')

    def __init__(self, *args, **kwargs):
        super(ExamQuestionSerializer, self).__init__(*args, **kwargs)
        if isinstance(self.instance, QuerySet):
            self.fields['selected_choice'] = serializers.PrimaryKeyRelatedField(
                allow_null=True,
                queryset=self.instance.select_related('question').select_related('choices'),
                required=True
            )
        elif isinstance(self.instance, ExamQuestion):
            self.fields['selected_choice'] = serializers.PrimaryKeyRelatedField(
                allow_null=True,
                queryset=self.instance.question.choices.all(),
                required=True
            )

    @staticmethod
    def get_question(obj):
        return QuestionSerializer(
            instance=obj.question,
            show_answers=obj.exam.is_finished,
            permutation_no=obj.permutation_no,
            read_only=True
        ).data

    def validate_selected_choice(self, choice):
        if isinstance(self.instance, ExamQuestion) and self.instance.exam.is_finished:
            raise serializers.ValidationError({
                'code': 'exam_already_completed',
                'detail': 'cannot change answers on finished exam'
            })
        return choice


class ExamQuestionListHyperlink(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            'exam_pk': obj.pk,
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)

    def get_object(self, view_name, view_args, view_kwargs):
        lookup_kwargs = {
            'exam': view_kwargs['exam_pk']
        }
        return self.get_queryset().get(**lookup_kwargs)


class ExamSerializer(serializers.ModelSerializer):
    questions_url = ExamQuestionListHyperlink(view_name='examquestion-list')

    class Meta:
        model = Exam
        fields = ('pk', 'url', 'start_time', 'end_time', 'score', 'questions_url', 'is_finished')
        read_only_fields = ('pk', 'url', 'start_time', 'end_time', 'score', 'questions_url')

    def __init__(self, *args, **kwargs):
        super(ExamSerializer, self).__init__(*args, **kwargs)
        if isinstance(self.instance, Exam) and self.instance.is_finished:
            self.fields['is_finished'].read_only = True


class ChoiceStatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChoiceStats
        fields = ('choice', 'times_selected')


class QuestionStatsSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='questionstat-detail',
        lookup_field='question_id',
        lookup_url_kwarg='pk',
        read_only=True
    )
    question = serializers.HyperlinkedRelatedField(
        view_name='questionstudy',
        lookup_field='pk',
        read_only=True
    )
    choicestats = serializers.SerializerMethodField()

    class Meta:
        model = QuestionStats
        fields = ('url', 'question', 'last_answered', 'times_correct', 'times_incorrect', 'choicestats')

    @staticmethod
    def get_choicestats(obj):
        all_choices = obj.question.choices.all()
        existing = obj.choicestats.all()
        missing = [ChoiceStats(choice=c, question_stats=obj) for c in all_choices if
                   c.id not in existing.values_list('choice_id', flat=True)]
        all = list(existing) + missing
        return ChoiceStatsSerializer(all, many=True).data
