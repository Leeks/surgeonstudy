from unittest import skip

from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase

from study.models import User
from util.jwt import jwt_creds


class AuthTests(APITestCase):
    sub = 'google-oauth2|112895318200147690672'

    def test_creates_user_if_not_exists(self):
        self.assertFalse(User.objects.count())
        self.client.credentials(**jwt_creds(self.sub))
        response = self.client.get(reverse('exam-list'))
        self.assertEqual(response.status_code, 200)

        # A user should have been created
        users = User.objects.all()
        self.assertEqual(users.count(), 1)
        user = users.first()

        # The user should have the right properties
        self.assertEqual(user.username, self.sub)
        self.assertFalse(user.is_superuser)
        self.assertFalse(user.is_staff)
