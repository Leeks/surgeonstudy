from unittest import mock

from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from study.models import Question
from study.serializers import QuestionSerializer
from study.stats import StatsManager
from util import test as testutils
from util.jwt import jwt_header


class QuestionStudyViewTests(testutils.TestDataMixin, testutils.AuthRequiredTestMixin, APITestCase):

    def get_auth_tests(self):
        q = Question.objects.first()
        return {
            reverse('questionstudy', args=[q.pk]): ['get']
        }

    def test_get_question(self):
        user = self.user_with_purchases
        q = Question.objects.for_user(user).first()
        url = reverse('questionstudy', args=[q.pk])
        self.client.credentials(**jwt_header(user))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 200)

    @mock.patch.object(StatsManager, 'question_answered')
    def test_reveal_answer(self, stats_manager_stub_func):
        user = self.user_with_purchases
        q = Question.objects.for_user(user).first()
        url = reverse('questionstudy', args=[q.pk])
        self.client.credentials(**jwt_header(user))
        r = self.client.post(url, {})
        expected_data = QuestionSerializer(instance=q, show_answers=True).data
        self.assertEqual(r.data, expected_data)
        self.assertFalse(stats_manager_stub_func.called)

    @mock.patch.object(StatsManager, 'question_answered')
    def test_submit_answer(self, stats_manager_stub_func):
        user = self.user_with_purchases
        q = Question.objects.for_user(user).first()
        c = q.choices.first()
        url = reverse('questionstudy', args=[q.pk])
        self.client.credentials(**jwt_header(user))
        r = self.client.post(url, {'choice': c.pk})
        expected_data = QuestionSerializer(instance=q, show_answers=True).data
        self.assertEqual(r.data, expected_data)
        stats_manager_stub_func.assert_called_with(q, c.pk)

