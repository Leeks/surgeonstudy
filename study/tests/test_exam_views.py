from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from rest_framework.reverse import reverse as rf_reverse
from rest_framework.test import APITestCase

from study.models import Exam, Choice
from util.jwt import jwt_header
from util.test import AuthRequiredTestMixin, TestDataMixin, StandardTestUtilsMixin, ExamSetupMixin


class ExamListTests(AuthRequiredTestMixin, TestDataMixin, StandardTestUtilsMixin, APITestCase):
    url = reverse('exam-list')
    auth_tests = {
        url: ['get', 'post']
    }

    def test_post_to_create_exam(self):
        self.client.credentials(**jwt_header(self.user_with_purchases))

        resp = self.client.post(self.url)

        self.assertEqual(resp.status_code, 201)

        # Exam is created
        exams = Exam.objects.filter(user=self.user_with_purchases.pk)
        self.assertEqual(exams.count(), 1)
        exam = exams.first()

        # Exam attrs
        self.assertEqual(exam.user, self.user_with_purchases)
        self.assertTrue(exam.start_time)
        self.assertFalse(exam.end_time)
        self.assertTrue(exam.questions.exists())

    def test_multiple_unfinished_raises_exception(self):
        Exam.objects.start_new(self.user_with_purchases)
        self.client.credentials(**jwt_header(self.user_with_purchases))
        resp = self.client.post(self.url)
        self.assertEqual(resp.status_code, 400)

    def test_list_own_exams_only(self):
        es = Exam.objects.start_new(self.user_staff)
        ep = Exam.objects.start_new(self.user_with_purchases)

        # Each user should have one exam belonging to themselves
        for u, e in [(self.user_staff, es), (self.user_with_purchases, ep)]:
            self.client.credentials(**jwt_header(u))
            resp = self.client.get(self.url)
            self.assertEqual(len(resp.data), 1)

            rel_url = rf_reverse('exam-detail', args=[e.pk])
            e = resp.data[0]
            self.assertDictContainsSubset(e, {'score': None, 'end_time': None})

            self.assertTrue(e['url'].endswith(rel_url))
            self.assertFalse(resp.data[0]['start_time'] is None)


class ExamDetailTests(AuthRequiredTestMixin, TestDataMixin, StandardTestUtilsMixin, ExamSetupMixin, APITestCase):
    @property
    def url(self):
        return reverse('exam-detail', args=[self.exam.pk])

    def get_auth_tests(self):
        return {
            self.url: ['get', 'put', 'post', 'patch'],
        }

    def test_get_exam(self):
        self.client.credentials(**jwt_header(self.user_with_purchases))
        resp = self.client.get(self.url)

        self.assertDictContainsSubset(resp.data, {
            'end_time': None,
            'score': None,
        })

        exam_pk = resp.data['pk']
        expected_url = 'http://testserver' + reverse('examquestion-list', kwargs={'exam_pk': exam_pk})
        self.assertEqual(resp.data['questions'], expected_url)

    def test_delete_unfinished_exam(self):
        self.client.credentials(**jwt_header(self.user_with_purchases))
        r = self.client.delete(self.url)
        with self.assertRaises(Exam.DoesNotExist):
            self.exam.refresh_from_db()
        self.assertEqual(r.status_code, 204)

    def test_cant_delete_finished_exam(self):
        self.client.credentials(**jwt_header(self.user_with_purchases))
        self.exam.is_finished = True
        self.exam.save()
        r = self.client.delete(self.url)
        self.assertNotEqual(r.status_code, 204)

    def test_finish_exam(self):
        self.client.credentials(**jwt_header(self.user_with_purchases))
        r = self.client.put(self.url, {'is_finished': True})
        self.assertEqual(r.status_code, 200, r.data)
        self.exam.refresh_from_db()
        self.assertTrue(self.exam.is_finished)


class ExamQuestionDetailTests(AuthRequiredTestMixin, TestDataMixin, StandardTestUtilsMixin, ExamSetupMixin,
                              APITestCase):
    def get_auth_tests(self):
        eqs = self.exam.examquestions.all()
        return {
            reverse('examquestion-detail', kwargs={'exam_pk': self.exam.pk, 'no': e.no}): ['post'] for e in eqs
            }

    def test_get_examquestion(self):
        """Get full exam question with GET"""
        q = self.exam.examquestions.get(no=1)
        url = reverse('examquestion-detail', kwargs={'exam_pk': self.exam.pk, 'no': q.no})
        choices = q.question.choices.all().values('id', 'text')
        self.client.credentials(**jwt_header(self.user_with_purchases))
        r = self.client.get(url)

        self.assertEqual(r.data['question']['prompt'], q.question.prompt)
        self.assertCountEqual(r.data['question']['choices'], choices)
        self.assertFalse(r.data['selected_choice'])

    def test_answer_examquestion(self):
        """Answer an exam question with POST"""
        q = self.exam.examquestions.get(no=1)
        c = q.question.choices.last()

        r = self._post_answer(self.exam, 1, c)
        self.assertEqual(r.status_code, 200)

        q.refresh_from_db()
        self.assertEqual(q.selected_choice, c)

    def test_cant_change_answers_on_finished_exam(self):
        self.exam.is_finished = True
        self.exam.save()
        r = self._post_answer(self.exam, 1)
        self.assertEqual(r.status_code, 400)

    def _post_answer(self, exam, question_no, choice=None, user=None):
        q = exam.examquestions.get(no=question_no)

        if not user:
            user = exam.user

        if not choice:
            choice_pk = q.question.choices.order_by('?').first().pk
        else:
            if isinstance(choice, int):
                choice_pk = choice
            else:
                choice_pk = choice.pk

        self.client.credentials(**jwt_header(user))
        url = reverse('examquestion-detail', kwargs={'exam_pk': exam.pk, 'no': question_no})

        return self.client.put(url, {'selected_choice': choice_pk})
