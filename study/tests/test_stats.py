from django.utils import timezone
from rest_framework.test import APITestCase

import util.test as testutils
from study.models import Question, QuestionStats
from study.stats import StatsManager


class StatsManagerTest(testutils.TestDataMixin, APITestCase):
    def setUp(self, *args, **kwargs):
        super(StatsManagerTest, self).setUp(*args, **kwargs)
        self.question = Question.objects.first()
        self.correct_choice = self.question.choices.get(is_correct=True)
        self.incorrect_choice = self.question.choices.filter(is_correct=False).first()

    def test_answer_tracking(self):
        choice = self.correct_choice
        for stats in self.answer_question(self.user_with_purchases, self.question, choice):
            for cs in stats.choicestats.all():
                if cs.choice == choice:
                    self.assertEqual(cs.times_selected, 1)
                else:
                    self.assertEqual(cs.times_selected, 0)

    def test_last_answered(self):
        for stats in self.answer_question(self.user_with_purchases, self.question, self.correct_choice):
            self.assertLess(stats.last_answered, timezone.now())

    def answer_question(self, user, question, choice):
        StatsManager(user).question_answered(question, choice)
        global_stats = QuestionStats.objects.get(user=QuestionStats.GLOBAL, question=question)
        user_stats = QuestionStats.objects.get(user=user, question=question)
        return user_stats, global_stats
