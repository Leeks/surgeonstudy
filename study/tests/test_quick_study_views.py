from unittest.mock import patch

from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase, APIRequestFactory

from bundle.models import Purchase
from study.models import Question, CurrentQuestion
from study.stats import StatsManager
from util.jwt import jwt_header
from util import test as testutils


class QuickStudyViewTest(testutils.TestDataMixin, testutils.AuthRequiredTestMixin, APITestCase):
    url = reverse('quickstudy')
    auth_tests = {
        url: ['get', 'post']
    }

    def setUp(self, *args, **kwargs):
        super(QuickStudyViewTest, self).setUp(*args, **kwargs)
        self.user = self.user_with_purchases
        self.question = Question.objects.first()
        self.selected_choice = self.question.choices.first()
        self.current_question_obj = CurrentQuestion.objects.create(user=self.user, question=self.question)
        self.client.credentials(**jwt_header(self.user))

    def test_get_question(self):
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, 200)
        self.assertTrue(r.data, 'Response has no data')
        self.assertCountEqual(r.data, ['prompt', 'category', 'choices'])
        for c in r.data['choices']:
            self.assertNotIn('is_correct', c)

    def test_superuser_or_staff_has_questions(self):
        """Superusers or staff can quickstudy even though they don't have purchases"""
        for user in [self.user_staff, self.user_super]:
            self.client.credentials(**jwt_header(user))
            self.assertFalse(Purchase.objects.filter(user=user).exists())
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, 200)

    def test_no_questions_available_404(self):
        """Returns 404 if the user doesn't have any question sets"""
        self.client.credentials(**jwt_header(self.user_without_purchases))
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.data, {'detail': 'User does not have access to any questions'})

    def test_valid_answer(self):
        """User answers question by posting."""
        r = self.client.post(self.url, {'choice': self.selected_choice.pk})
        self.assertEqual(r.status_code, 200)
        self.assertCountEqual(r.data, ['prompt', 'category', 'choices', 'selected_choice'])
        for c in r.data['choices']:
            self.assertIn('is_correct', c)

    def test_invalid_answer(self):
        r = self.client.post(self.url, {'choice': 999})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data, {'detail': 'Invalid choice'})

    @patch.object(StatsManager, 'question_answered', return_value=None)
    def test_answering_records_stats(self, question_answered_mock):
        r = self.client.post(self.url, {'choice': self.selected_choice.pk})
        self.assertEqual(r.status_code, 200)
        question_answered_mock.assert_called_once_with(self.current_question_obj.question, self.selected_choice.pk)

