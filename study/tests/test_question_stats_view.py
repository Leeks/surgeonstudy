from unittest import skip

from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIRequestFactory

from study.models import Question, QuestionStats
from study.serializers import QuestionStatsSerializer
from study.stats import StatsManager
from util import test as testutils
from util.jwt import jwt_header


class QuestionStatsListViewTest(testutils.TestDataMixin, testutils.AuthRequiredTestMixin, APITestCase):
    url = reverse('questionstat-list')
    auth_tests = {
        url: ['get', 'post']
    }

    def setUp(self, *args, **kwargs):
        super(QuestionStatsListViewTest, self).setUp(*args, **kwargs)
        self.user = self.user_with_purchases
        self.client.credentials(**jwt_header(self.user))

    def test_list_user_stats(self):
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, 200)
        qs = Question.objects.for_user(self.user)
        stats = [QuestionStats(user=self.user, question=q) for q in qs]
        s = QuestionStatsSerializer(stats, many=True, context={'request': APIRequestFactory().get(self.url)})
        self.assertCountEqual(r.data, s.data)


class QuestionStatsDetailViewTest(testutils.StandardTestUtilsMixin,
                                  testutils.TestDataMixin,
                                  testutils.AuthRequiredTestMixin,
                                  APITestCase):
    def get_auth_tests(self):
        return {
            reverse('questionstat-detail', args=[q.pk]): ['get'] for q in self.questions
            }

    def setUp(self, *args, **kwargs):
        super(QuestionStatsDetailViewTest, self).setUp(*args, **kwargs)
        self.user = self.user_with_purchases
        self.client.credentials(**jwt_header(self.user))
        self.unanswered_question = Question.objects.for_user(self.user).first()
        self.answered_question = Question.objects.for_user(self.user).last()
        self.choice = self.answered_question.choices.get(is_correct=True)
        StatsManager(self.user).question_answered(self.answered_question, self.choice)

    def test_creates_if_not_exist(self):
        q = self.unanswered_question

        with self.assertRaises(QuestionStats.DoesNotExist):
            QuestionStats.objects.get(user=self.user, question=q)

        r = self.client.get(reverse('questionstat-detail', args=[q.pk]))
        self.assertEqual(r.status_code, 200, r.data)

        try:
            QuestionStats.objects.get(user=self.user, question=q)
        except QuestionStats.DoesNotExist:
            self.fail('QuestionStats object not created')

    def test_data_payload(self):
        q = self.answered_question
        r = self.client.get(reverse('questionstat-detail', args=[q.pk]))

        self.assertEqual(r.status_code, 200)
        self.assertDictContainsSubset(
            r.data,
            {
                'times_correct': 1,
                'times_incorrect': 0,
            })

        self.assertTrue(r.data['url'].endswith(reverse('questionstat-detail', args=[q.pk])))
        self.assertTrue(r.data['question'].endswith(reverse('questionstudy', args=[q.pk])))
        expected_choicestats = [
            {'choice': choice.pk, 'times_selected': 1 if choice == self.choice else 0}
            for choice in self.answered_question.choices.all()
        ]
        self.assertCountEqual(r.data['choicestats'], expected_choicestats)
