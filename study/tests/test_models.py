from django.test import TestCase

from study.models import Category, Question, Choice, User
from util.test import StandardTestUtilsMixin


class TestExamQuestionModel(StandardTestUtilsMixin, TestCase):
    def setUp(self):
        self.user = User.objects.create(username='example|123', is_superuser=True, is_staff=True)

        self.category = Category.objects.create(name='Brain')
        self.question = Question.objects.create(prompt='What color is your gray matter?', category=self.category)
        self.question.choices.set([
            Choice(text='nasty', is_correct=True),
            Choice(text='blue'),
            Choice(text='orange'),
            Choice(text='lemony yellow'),
        ], bulk=False)