import csv
import string

from django.core.management.base import BaseCommand, CommandError

from study.models import Question, Category, Choice


class InvalidQuestion(Exception):
    def __init__(self, detail=None):
        self.detail = detail

    def __str__(self):
        return self.detail


def to_question_dict(row):
    if row[7].lower() == '':
        raise InvalidQuestion('no answer for question {}'.format(row[0]))

    return {
        'no': row[0],
        'prompt': row[1],
        'choices': row[2:7],
        'answer': string.ascii_lowercase.index(row[7].lower()),
        'category': row[8],
    }


class Command(BaseCommand):
    help = 'Imports questions from a file in csv format.'

    def add_arguments(self, parser):
        parser.add_argument('filename')

    def handle(self, *args, **options):
        with open(options['filename'], 'r', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile)
            next(reader)
            for row in reader:
                try:
                    qd = to_question_dict(row)
                except InvalidQuestion as e:
                    print(e.detail)
                    continue
                c, _ = Category.objects.get_or_create(name=qd['category'])
                q, _ = Question.objects.get_or_create(prompt=qd['prompt'], category=c)

                for index, text in enumerate(qd['choices']):
                    choice, _ = Choice.objects.get_or_create(text=text, question=q, is_correct=(index==qd['answer']))

