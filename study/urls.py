from django.conf.urls import url, include
from rest_framework import routers

from study.views.exam import ExamViewSet, ExamQuestionViewSet
from study.views.questionstudy import QuestionStudyViewSet
from study.views.stats import QuestionStatsViewSet
from study.views.quickstudy import QuickStudyView
from study.views.user import UserViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'exams', ExamViewSet, base_name='exam')
router.register(r'exams/(?P<exam_pk>[^/.]+)/questions', ExamQuestionViewSet, base_name='examquestion')
router.register(r'questionstats', QuestionStatsViewSet, base_name='questionstat')
router.register(r'questions', QuestionStudyViewSet, base_name='questionstudy')

urlpatterns = [
    url(r'^quickstudy/$', QuickStudyView.as_view(), name='quickstudy'),
    url(r'^', include(router.urls)),
]
