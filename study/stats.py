from django.utils import timezone

from study.models import Question, Choice, QuestionStats, ChoiceStats, GlobalQuestionStats, GlobalChoiceStats


class StatsManager(object):
    def __init__(self, user):
        self.user = user

    def question_answered(self, question, choice):
        if not isinstance(question, Question):
            question = Question.objecst.get(pk=question)

        if not isinstance(choice, Choice):
            choice = Choice.objects.get(pk=choice)


        # Global Stats
        global_stats, _ = GlobalQuestionStats.objects.get_or_create(question=question)

        global_choice_stats, _ = GlobalChoiceStats.objects.get_or_create(choice=choice, question_stats=global_stats)
        global_choice_stats.times_selected += 1

        if choice.is_correct:
            global_stats.times_correct += 1
        else:
            global_stats.times_incorrect += 1

        global_stats.last_answered = timezone.now()

        global_choice_stats.save()
        global_stats.save()

        # User stats
        user_stats, _ = QuestionStats.objects.get_or_create(user=self.user, question=question)

        user_choice_stats, _ = ChoiceStats.objects.get_or_create(choice=choice, question_stats=user_stats)
        user_choice_stats.times_selected += 1

        if choice.is_correct:
            user_stats.times_correct += 1
        else:
            user_stats.times_incorrect += 1

        user_stats.last_answered = timezone.now()

        user_choice_stats.save()
        user_stats.save()

