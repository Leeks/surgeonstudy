# surgeonstudy

## Production Requirements
- PostgreSQL
  - PostgreSQL-dev
  - Edit pg_hba.conf to allow md5 password login
- Environment Variables
  - DJ_PRODUCTION=True
  - DJ_DB_NAME
  - DJ_DB_USER
  - DJ_DB_PASSWORD

