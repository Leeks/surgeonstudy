from decimal import Decimal

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse

from djstripe.models import Customer

from bundle.models import Bundle, Purchase


def is_purchased(product, user):
    return product.purchase_set.filter(user=user).exists()

class NoPurchases(TemplateView):
    template_name = 'payments/no-purchases.html'

class ProductsView(TemplateView):
    template_name = 'payments/products.html'

    def get_context_data(self, **kwargs):
        context = super(ProductsView, self).get_context_data(**kwargs)
        user = self.request.user
        context['products'] = Bundle.objects.all()

        # TODO collapse to single query
        for product in context['products']:
            if user.is_anonymous():
                product.is_purchased = False
            else:
                product.is_purchased = is_purchased(product, self.request.user)

        return context


class PurchaseView(TemplateView):
    template_name = 'payments/purchase.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PurchaseView, self).dispatch(*args, **kwargs)

    def get(self, request, product):
        product = Bundle.objects.get(slug=product)

        if is_purchased(product, request.user):
            return redirect(reverse('products'))

        customer, new_customer = Customer.get_or_create(
            subscriber=request.user
        )

        # TODO maybe we can remove this sync
        customer.sync()

        if customer.can_charge():
            return render(
                request,
                self.template_name,
                {
                    'customer': customer,
                    'product': product
                }
            )
        else:
            redirect_url = reverse('djstripe:change_card')
            return redirect(redirect_url + '?next=' + request.path)

    def post(self, request, product):
        customer = Customer.objects.get(
            subscriber=request.user
        )

        product = Bundle.objects.get(slug=product)

        charge_amount = Decimal(product.price)
        customer.charge(charge_amount)

        Purchase.objects.create(user=request.user, product=product)

        # TODO email confirmation, order
        return redirect('dashboard')
