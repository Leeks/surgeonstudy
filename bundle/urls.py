from django.conf.urls import url

from .views import ProductsView, PurchaseView, NoPurchases

urlpatterns = [
    url(r'^$', ProductsView.as_view(), name='products'),
    url(r'^no-purchases/$', NoPurchases.as_view(), name='no-purchases'),
    url(r'^purchase/(?P<product>[a-zA-Z0-9-]+)/$', PurchaseView.as_view(), name='purchase'),
]
