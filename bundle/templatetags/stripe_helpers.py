
from django import template
from django.http.request import HttpRequest
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()

@register.filter
def cents_to_dollars(cents: int) -> str:
    return '{0:.02f}'.format(float(cents) / 100.0)
