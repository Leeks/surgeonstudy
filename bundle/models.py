import uuid

from django.conf import settings
from django.db import models
from django.utils import timezone

from study.models import QuestionSet

class Bundle(models.Model):
    name = models.TextField()
    slug = models.SlugField()
    description = models.TextField()
    price = models.PositiveIntegerField()
    questionsets = models.ManyToManyField(QuestionSet, related_name='bundles')

    def __str__(self):
        return self.name


class Purchase(models.Model):
    order_no = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    bundle = models.ForeignKey('Bundle', on_delete=models.PROTECT, related_name='purchases')
    time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '{}: {} - {}'.format(self.order_no, self.user, self.bundle)