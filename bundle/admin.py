from django.contrib import admin

from .models import Bundle, Purchase


class BundleAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(Bundle, BundleAdmin)

admin.site.register(Purchase)
