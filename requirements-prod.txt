-r requirements-base.txt

aiohttp==0.21.6
gunicorn==19.6.0
chardet==2.3.0