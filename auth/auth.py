from datetime import timedelta
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone
from email.utils import parsedate_to_datetime
from rest_framework import status
from rest_framework.exceptions import ValidationError, APIException
from rest_framework.authentication import BaseAuthentication, get_authorization_header
import requests
from cryptography.x509 import load_pem_x509_certificate
from cryptography.hazmat.backends import default_backend
from jwt.api_jws import get_unverified_header
import jwt


class ServerError(APIException):
    status_code = 500


class FirebaseKeys(object):
    def __init__(self):
        self.__data__ = {}
        self.__expires__ = timezone.now()

    def update_keys(self):
        r = requests.get('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com')
        r.raise_for_status()

        self.__expires__ = parsedate_to_datetime(r.headers['Expires'])

        content = r.json()
        self.__data__ = {
            cert: load_pem_x509_certificate(bytes(content[cert], 'UTF-8'), default_backend()).public_key()
            for cert in content
        }

    def __getitem__(self, item):
        if timezone.now() >= self.__expires__:
            self.update_keys()

        return self.__data__[item]


class AuthFailed(ValidationError):
    status_code = status.HTTP_401_UNAUTHORIZED


class FirebaseAuthentication(BaseAuthentication):
    keys = FirebaseKeys()

    def authenticate(self, request):
        auth_header = get_authorization_header(request).split()

        if not auth_header:
            return None

        if auth_header[0].lower() != b'bearer':
            return None
        elif len(auth_header) == 1:
            raise AuthFailed({
                'code': 'invalid_header',
                'description': 'token not found',
            })
        elif len(auth_header) > 2:
            raise AuthFailed({
                'code': 'invalid_header',
                'description': 'authorization header must be Bearer + \s + token',
            })

        token = auth_header[1]

        try:
            header = get_unverified_header(token)
        except jwt.DecodeError:
            raise AuthFailed({'code': 'invalid_token', 'description': 'token could not be decoded'})
        except jwt.InvalidTokenError:
            raise AuthFailed({'code': 'invalid_kid', 'description': 'token has invalid kid'})

        try:
            key = self.keys[header['kid']]
        except KeyError:
            raise AuthFailed({'code': 'invalid_kid', 'description': 'token has invalid kid'})

        try:
            payload = jwt.decode(
                token,
                key,
                audience=settings.FIREBASE_PROJECT_ID,
                issuer='https://securetoken.google.com/' + settings.FIREBASE_PROJECT_ID,
                leeway=timedelta(seconds=10),
            )
        except jwt.ExpiredSignature:
            raise AuthFailed({'code': 'token_expired', 'description': 'token is expired'})
        except jwt.InvalidAudienceError:
            raise AuthFailed({
                'code': 'invalid_audience',
                'description': 'incorrect audience, expected: ' + settings.FIREBASE_PROJECT_ID,
            })
        except jwt.InvalidIssuer:
            raise AuthFailed({
                'code': 'invalid_audience',
                'description': 'incorrect issuer, expected: https://securetoken.google.com/' + settings.FIREBASE_PROJECT_ID,
            })
        except jwt.DecodeError:
            raise AuthFailed({'code': 'token_invalid_signature', 'description': 'token signature is invalid'})

        if payload['sub'] != payload['user_id']:
            raise AuthFailed({'code': 'user_id_mismatch', 'description': 'the subject and user id do not match'})

        name = [s.strip() for s in payload.get('name').split()]

        try:
            first_name = name[0]
        except IndexError:
            first_name = None
        except AttributeError:
            first_name = None

        try:
            last_name = name[-1]
        except IndexError:
            last_name = None
        except AttributeError:
            last_name = None
        finally:
            if first_name == last_name:
                last_name = None

        user, created = get_user_model().objects.update_or_create(
            username=payload['user_id'],
            defaults={
                'email': payload['email'],
                'first_name': first_name,
                'last_name': last_name
            }
        )

        return user, payload

    def authenticate_header(self, request):
        return 'Bearer'
