defusedxml==0.4.1
Django==1.10
psycopg2==2.6.2
sqlparse==0.2.0

django-model-utils>=2.5
django-braces>=1.8.1
jsonfield>=1.0.3
pytz>=2016.6

django-html_sanitizer>=0.1.5
django-ckeditor>=5.1.0
django-cors-middleware>=1.3.1

cryptography==1.4
PyJWT==1.4.1
requests==2.11.0
six==1.10.0

djangorestframework==3.4.3
django-filter==0.13.0
django-crispy-forms==1.6.0

braintree==3.29.2
